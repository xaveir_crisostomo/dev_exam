<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB, Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'username' => 'master_admin',
            'password' => Hash::make('admin'),
        ]);
    }
}
