<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use DB;

class CategoryController extends Controller
{
    protected $data;

    public function index(){
        $this->data['categories'] = Category::orderBy('id')->get();

        return view('pages.category.index',$this->data);
    }

    public function create(){
        return view('pages.category.create');
    }

    public function store(CategoryRequest $request){
        $category = new Category();
        
        try {
            DB::beginTransaction();
            $category_name = $request->get('category_name');
            $category_desc = $request->get('category_desc');

            $category->category_name = $category_name;
            $category->category_desc = $category_desc;
            $category->save();

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            return dd("error: ". $e);
        }

        return redirect()->route('category.index');
        
    }

    public function edit($id){
        $this->data['category'] = Category::where('id',$id)->first();
        return view('pages.category.edit',$this->data);
    }

    public function update(CategoryRequest $request, $id){
        $category = Category::where('id',$id)->first();
        
        try {
            DB::beginTransaction();
            $category_name = $request->get('category_name');
            $category_desc = $request->get('category_desc');

            $category->category_name = $category_name;
            $category->category_desc = $category_desc;
            $category->save();

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            return dd("error: ". $e);
        }

        return redirect()->route('category.index');
        
    }

    public function delete($id){
        $category = Category::where('id',$id)->first();
        $category->delete();
        return redirect()->route('category.index');
    }

    public function search(Request $request){
        $category_name = $request->get('category_search');
        $category = Category::where('category_name','LIKE',$category_name)->first();

        if($category == null){
            $this->data['categories'] = array();
        }
        else{
            $this->data['categories'] = Category::where('id',$category->id)->get();
        }

        return view('pages.category.search',$this->data);
    }
}
