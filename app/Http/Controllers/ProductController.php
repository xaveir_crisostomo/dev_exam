<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Http\Requests\ProductRequest;
use DB;

class ProductController extends Controller
{
    protected $data;

    public function index(){
        $this->data['products'] = Product::orderBy('id')->get();
        return view('pages.product.index',$this->data);
    }

    public function create(){
        $this->data['categories'] = Category::orderBy('id')->get();
        return view('pages.product.create',$this->data);
    }

    public function store(ProductRequest $request){
        $category = Category::where('id',$request->get('product_category'))->first();
        $product = new Product();
        try {
            
            $product_name = $request->get('product_name');
            $category_id = $request->get('product_category');
            $product_category = $category->category_name;
            $product_desc = $request->get('product_desc');
            $image_path = "images/".$product_category;
            $image_name = time().'.'.$request->file('product_image')->extension();
            
            $product->product_name = $product_name;
            $product->category_id = $category_id;
            $product->product_description = $product_desc;
            $product->image_path = $image_path."/".$image_name;
            $product->save();

            $request->file('product_image')->move(public_path($image_path),$image_name);

        } catch (\Throwable $th) {
            DB::rollback();
            return dd("error: Upload Failed");
        }

        return redirect()->route('product.index');
    }

    public function edit($id){
        $this->data['categories'] = Category::orderBy('id')->get();
        $this->data['product'] = Product::where('id',$id)->first();
        return view('pages.product.edit',$this->data);
    }

    public function update(ProductRequest $request, $id){
        $category = Category::where('id',$request->get('product_category'))->first();
        $product =  Product::where('id',$id)->first();
        try {
            
            $product_name = $request->get('product_name');
            $category_id = $request->get('product_category');
            $product_category = $category->category_name;
            $product_desc = $request->get('product_desc');
            $image_path = "images/".$product_category;
            $image_name = time().'.'.$request->file('product_image')->extension();
            
            $product->product_name = $product_name;
            $product->category_id = $category_id;
            $product->product_description = $product_desc;
            $product->image_path = $image_path."/".$image_name;
            $product->save();

            $request->file('product_image')->move(public_path($image_path),$image_name);

        } catch (\Throwable $th) {
            DB::rollback();
            return dd("error: Upload Failed");
        }

        return redirect()->route('product.index');
    }

    public function delete($id){
        $product = Product::where('id',$id)->get();
        $product->delete();
        return redirect()->route('product.index');
    }

    public function search(Request $request){
        $product_name = $request->get('search');
        $this->data['products'] = Product::where('product_name','LIKE','%'.$product_name.'%')->get();

        return view('pages.product.search',$this->data);
    }

    public function categorySearch(Request $request){
        $category_name = $request->get('category_search');
        $category = Category::where('category_name','LIKE',$category_name)->first();
        
        if($category == null){
            $this->data['products'] = array();
        }
        else{
            $this->data['products'] = Product::where('category_id',$category->id)->get();
        }

        return view('pages.product.search',$this->data);
    }
}
