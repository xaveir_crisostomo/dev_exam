<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middlware' => "auth"], function(){
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::group(['as' => "category.", 'prefix' => "category"], function(){
        Route::get('/',[CategoryController::class,'index'])->name('index');
        Route::get('/create',[CategoryController::class,'create'])->name('create');
        Route::post('/create',[CategoryController::class,'store'])->name('store');
        Route::get('/edit/{id}',[CategoryController::class,'edit'])->name('edit');
        Route::post('/edit/{id}',[CategoryController::class,'update'])->name('update');
        Route::any('/delete/{id}',[CategoryController::class,'delete'])->name('delete');
        Route::get('/search',[CategoryController::class,'search'])->name('search');
    });

    Route::group(['as' => "product.", 'prefix' => "products"], function(){
        Route::get('/',[ProductController::class,'index'])->name('index');
        Route::get('/create',[ProductController::class,'create'])->name('create');
        Route::post('/create',[ProductController::class,'store'])->name('store');
        Route::get('/edit/{id}',[ProductController::class,'edit'])->name('edit');
        Route::post('/edit/{id}',[ProductController::class,'update'])->name('update');
        Route::any('/delete/{id}',[ProductController::class,'delete'])->name('delete');
        Route::get('/search',[ProductController::class,'search'])->name('search');
        Route::get('/search/category',[ProductController::class,'categorySearch'])->name('category.search');
    });

});

Route::any('/logout',[LoginController::class,'logout'])->name('logout');

Auth::routes();




