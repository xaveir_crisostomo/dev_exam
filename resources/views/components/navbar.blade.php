<div class="navbar-container">
    <div class="nav-items">
        <button class="minimize-btn" onclick="minimize()"><i class="fas fa-bars"></i></button>

        <form action="{{route('product.search')}}">
            @csrf
            <input type="text" name="search" placeholder="Search Products">
        </form>
    </div>
</div>

