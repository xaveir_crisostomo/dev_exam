<div class="sidebar-container">
    <div id="logo" class="logo-container">
        Backend Dev Exam PRAXXYS
    </div>
    <div class="user-container">
        <div class="user-image"><img src="{{asset('images/person.png')}}" alt=""></div>
        <span id="sideText" >{{Auth::user()->name}}</span>
    </div>
    <div class="sidebar-items">
        <ul>
            <li><a href="{{route('home')}}"><i class="fa fa-home"></i> <span id="sideText1"> Home</span></a></li>
            <li><a href="{{route('category.index')}}"><i class="fas fa-cubes"></i> <span id="sideText2"> Categories</span></a></li>
            <li><a href="{{route('product.index')}}"><i class="fas fa-tshirt"></i> <span id="sideText3"> Products</span></a></li>
            <li><a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> <span id="sideText4"> Logout</span></a>
            </li>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </ul>
    </div>
</div>