@extends('layouts.app')
@section('content')
<div class="content-container">
    <div class="content-title">
        <span>Home</span>
    </div>

    <div class="content-text">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
        Nunc posuere purus in eros convallis, at condimentum magna aliquam. 
        Nullam venenatis erat sed metus malesuada convallis. Fusce eu tristique tellus. 
        Vestibulum tincidunt tortor risus, a interdum nibh accumsan ut. 
        Morbi viverra ex eget ultricies blandit. Etiam sagittis dui orci, 
        eu hendrerit nisi viverra non. Nunc eu convallis quam. Nam volutpat nisi est, 
        eu sagittis sem porttitor quis. Etiam vel eros cursus, maximus est in, ultrices orci. 
        Donec ultrices libero sed nibh accumsan, eu mattis orci laoreet. Etiam a lorem nec velit 
        interdum dapibus.
    </div>
</div>
@endsection