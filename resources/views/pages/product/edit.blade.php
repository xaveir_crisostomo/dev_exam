@extends('layouts.app')
@section('content')
<div class="content-container">
    <div class="content-title">
        <h3>Product</h3>

        <div class="content-navigation">
            <a href="{{route('home')}}">Home</a> / 
            <a href="{{route('product.index')}}">Product</a> / 
            Edit
        </div>
    </div>
    <div class="content-form-container">
        <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-title">
                Edit Product
            </div>
            <div class="form-input">
                <label for="product_category">Product name</label>
                <select name="product_category" id="">
                    <option value="" selected disabled>Select Category</option>
                    @forelse($categories as $category)
                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                    @empty
                    <option value="" disabled>No Categories</option>
                    @endforelse
                </select> 
                @if ($errors->has('product_category'))
                    <span class="text-danger">{{$errors->first()}}</span>
                @endif
            </div>
            <div class="form-input">
                <label for="product_name">Product name</label>
                <input name="product_name" type="text" placeholder="Enter Product Title" value="{{$product->product_name}}">
                @if ($errors->has('product_name'))
                    <span class="text-danger">{{$errors->first()}}</span>
                @endif
            </div>
            <div class="form-input">
                <label for="product_desc">Product Details</label>
                <textarea name="product_desc" id="" cols="20" rows="5" placeholder="Enter Product Title">{{$product->product_description}}</textarea>
                @if ($errors->has('product_desc'))
                    <span class="text-danger">{{$errors->first()}}</span>
                @endif
            </div>
            <div class="form-input">
                <label for="product_image">Product Image</label>
                <input name="product_image" type="file">
                @if ($errors->has('product_image'))
                    <span class="text-danger">{{$errors->first()}}</span>
                @endif
            </div>
            <div class="form-button">
                <button type="submit">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection