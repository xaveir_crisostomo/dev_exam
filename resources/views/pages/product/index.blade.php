@extends('layouts.app')
@section('content')
<div class="content-container">
    <div class="content-title">
        <span>Product</span>

        <div class="content-navigation">
            <a href="{{route('home')}}">Home</a> / Product
        </div>
    </div>
    <div class="create-button">
        <a href="{{route('product.create')}}">Create Product</a>
    </div>
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Products Table</h3>
        <form action="{{route('product.category.search')}}">
            @csrf
            <input type="text" name="category_search" placeholder="Search Category">
        </form>
      </div>
      <!-- /.card-header -->
      <div class="card-body p-0">
        <table class="table table-striped">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Image</th>
              <th>Title</th>
              <th>Description</th>
              <th>Category</th>
              <th style="width: 40px">Action</th>
            </tr>
          </thead>
          <tbody>
            @forelse($products as $product)
            <tr class="product-tr">
              <td>{{$product->id}}</td>
              <td>
                <img src="{{asset($product->image_path ? $product->image_path : null)}}" height="50px" alt="{{$product->image_path ? $product->image_path : null}}">
              </td>
              <td>{{$product->product_name ? $product->product_name : 'Deleted'}}</td>
              <td>{{$product->product_description ? $product->product_description : 'Deleted'}}</td>
              <td>{{$product->category->category_name ? $product->category->category_name : 'Deleted'}}</td>
              <td>
                <a href="{{route('product.edit',$product->id)}}">Edit</a>
                <a href="{{route('product.delete',$product->id)}}">Delete</a>
              </td>
            </tr>
            @empty
            <tr>
              <td colspan="6" style="text-align:center">No Products Yet</td>
            </tr>
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
</div>
@endsection