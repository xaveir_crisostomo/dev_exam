@extends('layouts.app')
@section('content')
<div class="content-container">
    <div class="content-title">
        <h3>Category</h3>

        <div class="content-navigation">
            <a href="{{route('home')}}">Home</a> / 
            <a href="{{route('category.index')}}">Category</a> / 
            Create
        </div>
    </div>
    <div class="content-form-container">
        <form action="" method="post">
            @csrf
            <div class="form-title">
                Add Category
            </div>
            <div class="form-input">
                <label for="category_name">Category Title</label>
                <input name="category_name" type="text" placeholder="Enter Category Title" value="{{old('category_name')}}">
                @if ($errors->has('category_name'))
                    <span class="text-danger">{{$errors->first()}}</span>
                @endif
            </div>
            <div class="form-input">
                <label for="category_desc">Product Details</label>
                <textarea name="category_desc" id="" cols="20" rows="5" placeholder="Enter Product Title" value="{{old('category_desc')}}"></textarea>
                @if ($errors->has('category_desc'))
                    <span class="text-danger">{{$errors->first()}}</span>
                @endif
            </div>
            <div class="form-button">
                <button type="submit">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection