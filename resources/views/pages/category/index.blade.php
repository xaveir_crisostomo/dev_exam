@extends('layouts.app')
@section('content')
<div class="content-container">
    <div class="content-title">
        <span>Category</span>

        <div class="content-navigation">
            <a href="{{route('home')}}">Home</a> / Category
        </div>
    </div>
    <div class="create-button">
        <a href="{{route('category.create')}}">Create Category</a>
    </div>
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Categories Table</h3>
                <form action="{{route('category.search')}}">
                    @csrf
                    <input type="text" name="category_search" placeholder="Search Category">
                </form>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($categories as $category)
                    <tr>
                      <td>{{$category->id ? $category->id : 'Category Deleted'}}</td>
                      <td>{{$category->category_name ? $category->category_name : 'Category Deleted'}}</td>
                      <td>{{$category->category_desc ? $category->category_desc : 'Category Deleted'}}</td>
                      <td>
                        <a href="{{route('category.edit',$category->id)}}">Edit</a>
                        <a href="{{route('category.delete',$category->id)}}">Delete</a>
                      </td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="4" style="text-align:center">No Categories Yet</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
</div>
@endsection