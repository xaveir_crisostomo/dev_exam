<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;600&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <main id="mainContainer" class="main-container">
            <div class="sidebar">
                @include('components.sidebar')
            </div>
            <div class="content">
                @include('components.navbar')
                @yield('content')
            </div>
        </main>
    </div>
    <script>
        let isMinimized = false;
        function minimize(){
            if(isMinimized === false){
                document.getElementById('mainContainer').style.gridTemplateColumns = "5% 95%";
                document.getElementById('logo').style.display = "none";
                document.getElementById('sideText').style.display = "none";
                document.getElementById('sideText1').style.display = "none";
                document.getElementById('sideText2').style.display = "none";
                document.getElementById('sideText3').style.display = "none";
                document.getElementById('sideText4').style.display = "none";

                isMinimized = true;
            }
            else{
                document.getElementById('mainContainer').style.gridTemplateColumns = "20% 80%";
                document.getElementById('logo').style.display = "flex";
                document.getElementById('sideText').style.display = "inline-block";
                document.getElementById('sideText1').style.display = "inline-block";
                document.getElementById('sideText2').style.display = "inline-block";
                document.getElementById('sideText3').style.display = "inline-block";
                document.getElementById('sideText4').style.display = "inline-block";

                isMinimized = false;
            }
        }
    </script>
</body>
</html>
